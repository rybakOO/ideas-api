import { Injectable, CacheInterceptor, ExecutionContext, Logger } from "@nestjs/common";


@Injectable()
export default class HttpCacheInterceptor extends CacheInterceptor {
  trackBy(context: ExecutionContext): string | undefined {
    const { method, url } = context.switchToHttp().getRequest();
    Logger.log(`${method}, ${url}`, 'HttpCacheInterceptor');
    return "key"
  }
}