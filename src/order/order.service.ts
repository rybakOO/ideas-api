import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Order } from '../types/order';
import { CreateOrderDTO } from './order.dto';

@Injectable()
export class OrderService {
  constructor(@InjectModel('Order') private orderModel: Model<Order>) {}

  async listOrdersByUser(userId: string) {
    return await this.orderModel.find({ owner: userId})
      .populate('owner')
      .populate('products.product');
  }

  async createOrder(orderDTO: CreateOrderDTO, userId: string) {
    const order = await this.orderModel.create({
      owner: userId,
      products: orderDTO.products,
      totalPrice: orderDTO.products.reduce((acc, product) => 
         acc + product.price * product.quantity
      , 0)
    });
    await order.save();
    return await this.orderModel.findById(order.id)
      .populate('owner')
      .populate('products.product');
  }
}
