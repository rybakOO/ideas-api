import { Controller, Get, UseGuards, Body, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { OrderService } from './order.service';
import { User as UserDocument } from '../types/user';
import { User } from '../utilities/user.decorator';
import { Order } from '../types/order';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async listOrder(@User() {id}: UserDocument) {
    return await this.orderService.listOrdersByUser(id);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async createOrder(@User() {id}: UserDocument, @Body() order: Order) {
    return await this.orderService.createOrder(order, id);
  }
}
