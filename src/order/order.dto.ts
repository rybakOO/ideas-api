import { ProductOrder } from '../types/order';


export interface CreateOrderDTO {
  products: ProductOrder[];
}