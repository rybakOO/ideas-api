import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const {user} = context.switchToHttp().getRequest();
    if(user && Array.isArray(user.roles)) {
      return roles.every((role) => user.roles.includes(role));
    } else {
      throw new UnauthorizedException();
    }
  }
}