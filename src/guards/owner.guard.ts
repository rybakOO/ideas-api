import { Injectable, Inject, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import { ProductService } from '../product/product.service';

@Injectable()
export class OwnerGuard implements CanActivate {
  constructor(@Inject('ProductService') private readonly productService: ProductService) {}
  async canActivate(context: ExecutionContext) {
    const { user, params } = context.switchToHttp().getRequest();
    const { owner } = await this.productService.findById(params.id);
    if(user.id !== owner.id) {
      throw new HttpException('You do not own this product', HttpStatus.UNAUTHORIZED);
    }
    return true;
  }
}