import { Controller, Get, Post, Delete, Body, Param, UseGuards, Put, ValidationPipe, UsePipes, Scope } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductService } from './product.service';
import { CreateProductDTO, UpdateProductDTO } from '../product/product.dto';
import { SellerGuard } from '../guards/seller.guard';
import { OwnerGuard } from '../guards/owner.guard';
import { User } from '../utilities/user.decorator';
import { User as UserDocument } from '../types/user';
import { Product } from '../types/product';

// @Controller({ path: 'product', scope: Scope.REQUEST })
@Controller({ path: 'product', scope: Scope.DEFAULT })
export class ProductController {
  constructor(private productService: ProductService) {}

  // count = 0

  @Get()
  async listAll(): Promise<Product[]> {
    return await this.productService.findAll();
  }

  @Get('/mine')
  @UseGuards(AuthGuard('jwt'), SellerGuard)
  async listMine(@User() user: UserDocument): Promise<Product[]> {
    const { id } = user;
    return await this.productService.findByOwner(id);
  }

  @Get('/seller/:id')
  @UseGuards(AuthGuard('jwt'), SellerGuard)
  async listBySeller(@Param('id') id: string):Promise<Product[]> {
    return await this.productService.findByOwner(id);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'), SellerGuard)
  @UsePipes(ValidationPipe)
  async create(@Body() product: CreateProductDTO, @User() user: UserDocument) :Promise<Product> {
    // console.log(++this.count)
    return await this.productService.create(product, user);
  }

  @Get(':id')
  async read(@Param('id') id: string):Promise<Product> {
    return await this.productService.findById(id);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), SellerGuard, OwnerGuard)
  async update(
      @Param('id') id: string, 
      @Body() product: UpdateProductDTO, 
    ):Promise<Product> {
      return await this.productService.update(id, product);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'), SellerGuard, OwnerGuard)
  async delete(@Param('id') id: string):Promise<Product> {
    return await this.productService.delete(id);
  }
}
