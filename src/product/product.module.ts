import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ProductSchema } from '../models/product.schema';

@Module({
  imports: [
    // MongooseModule.forFeature([{name: 'Product', schema: ProductSchema}]),
    MongooseModule.forFeatureAsync([
      {
        name: 'Product',
        useFactory: () => {
          const schema = ProductSchema;
          schema.pre('save', () => console.log('Hello from pre save'));
          schema.post('save', () => console.log('Hello from post save'));
          return schema;
        },
      },
    ]),
  ],
  controllers: [ProductController],
  providers: [ProductService]
})
export class ProductModule {}
