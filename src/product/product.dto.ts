import { IsString, IsDefined, IsNumberString } from 'class-validator';

export class CreateProductDTO {
  @IsDefined()
  @IsString()
  readonly title: string
  @IsDefined()
  @IsString()
  readonly image: string;
  @IsDefined()
  @IsString()
  readonly description: string;
  @IsNumberString()
  readonly price: number;
}

export type UpdateProductDTO = Partial<CreateProductDTO>;