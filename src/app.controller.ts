import { Controller, Get, CacheKey, CacheTTL } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @CacheKey('app_controller')
  @CacheTTL(5)
  @Get()
  getHello(): string {
    const acc = Array(20000000).fill(10).reduce((acc, item) => acc + item, 0)
    return `${this.appService.getHello()} ${acc}`;
  }
}
