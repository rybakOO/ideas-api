import { Document } from 'mongoose';
import { Exclude } from 'class-transformer';

export interface Address {
  addr1: string;
  addr2: string;
  city: string;
  state: string;
  country: string;
  zip: number
}

export class User extends Document {
  username: string;
  readonly password: string;
  seller: boolean;
  roles?: string[];
  address: Address;
  created: Date
} 

export class UserEntity {
  username: string;
  @Exclude()
  password: string;
  seller: boolean;

  token?: string;

  constructor(partial: Partial<UserEntity>) {
    Object.assign(this, partial);
  }
} 