export interface Service {
  findById: (id: string) => Promise<object>
}