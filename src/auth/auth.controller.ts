import { Controller, Post, Body, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from '../shared/user.service';
import { LoginDTO, RegisterDTO } from './auth.dto';
import { Payload } from '../types/payload';
import { AuthService } from './auth.service';
import { User } from '../utilities/user.decorator';
import { SellerGuard } from '../guards/seller.guard';
import { RolesGuard } from '../guards/roles.guard';
import { Roles } from '../utilities/roles.decorator';

@Controller('auth')
export class AuthController {
  constructor(private userService: UserService, private authService: AuthService) {}

  @Get()
  @Roles('admin')
  @UseGuards(AuthGuard('jwt'), RolesGuard, SellerGuard)
  async findAll(@User() user: any) {
    return await this.userService.findAll();
  }

  @Post('login')
  async login(@Body() userDTO: LoginDTO) {
    const user = await this.userService.findByLogin(userDTO);
    const payload: Payload = {
      username: user.username,
      seller: user.seller
    };
    const token = await this.authService.signPayload(payload);
    return { user, token };
  }

  @Post('register')
  async register(@Body() userDTO: RegisterDTO) {
    const user = await this.userService.create(userDTO);
    const payload: Payload = {
      username: user.username,
      seller: user.seller
    };
    const token = await this.authService.signPayload(payload);
    return { user, token };
  }
}
