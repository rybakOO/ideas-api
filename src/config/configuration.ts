export default () => ({
  port: parseInt(process.env.PORT, 10),
  mongoURI: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TEST : process.env.MONGO_URI
})