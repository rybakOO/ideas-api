import * as request from 'supertest';
import * as mongoose from 'mongoose';
import axios from 'axios';
import { HttpStatus } from '@nestjs/common';
import { RegisterDTO } from '../src/auth/auth.dto';
import { CreateProductDTO } from '../src/product/product.dto';
import { app, database  } from './constants';

let sellerToken: string;
const productSeller: RegisterDTO = {
  seller: true,
  username: 'productSeller',
  password: 'password'
};

beforeAll(async () => {
  await mongoose.connect(database);
  await mongoose.connection.db.collection('users').remove({});
  await mongoose.connection.db.collection('products').remove({});
  const {data: {token}} = await axios.post(`${app}/auth/register`, productSeller);
  sellerToken = token;
});

afterAll(async (done) => {
  await mongoose.connection.db.collection('users').remove({});
  await mongoose.connection.db.collection('products').remove({});
  await mongoose.disconnect(done);
});

describe('PRODUCT', () => {

  const product: CreateProductDTO = {
    title: 'new phone',
    description: 'description',
    price: 10,
    image: 'n/a'
  };
  let productId:string;

  it('should respect seller token', () => {
    return request(app)
      .get('/product/mine')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${sellerToken}`)
      .expect(HttpStatus.OK)
  });

  it('should create product', () => {
    return request(app)
      .post('/product')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${sellerToken}`)
      .send(product)
      .expect(({body}) => {
        productId = body._id;
        expect(body.title).toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.image).toEqual(product.image);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(HttpStatus.CREATED)
  });

  it('should read product', () => {
    return request(app)
      .get(`/product/${productId}`)
      .expect(({body}) => {
        expect(body._id).toEqual(productId);
        expect(body.title).toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.image).toEqual(product.image);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(HttpStatus.OK)
  });

  it('should update product', () => {
    return request(app)
      .put(`/product/${productId}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${sellerToken}`)
      .send({title: 'new title'})
      .expect(({body}) => {
        expect(body._id).toEqual(productId);
        expect(body.title).not.toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.image).toEqual(product.image);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(HttpStatus.OK)
  });

  it('should delete product', async () => {
    await axios.delete(`${app}/product/${productId}`, {
      headers: {
        'Authorization': `Bearer ${sellerToken}`
      }
    });
    
    return request(app)
      .get(`/product/${productId}`)
      .expect(HttpStatus.NO_CONTENT)
  });
});