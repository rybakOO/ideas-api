import * as request from 'supertest';
import * as mongoose from 'mongoose';
import axios from 'axios';
import { RegisterDTO } from '../src/auth/auth.dto';
import { CreateProductDTO } from 'src/product/product.dto';
import { Product } from 'src/types/product';
import { app, database  } from './constants';
import { HttpStatus } from '@nestjs/common';

let sellerToken: string;
let buyerToken: string;
let orderId: string;
let boughtProducts: Product[];

const orderBuyer: RegisterDTO = {
  seller: false,
  username: 'orderBuyer',
  password: 'password'
};
const orderSeller: RegisterDTO = {
  seller: true,
  username: 'productSeller',
  password: 'password'
}
const soldProducts: CreateProductDTO[] = [
  {
    title: 'newer phone',
    image: 'n/a',
    description: 'description',
    price: 1000
  },
  {
    title: 'newest phone',
    image: 'n/a',
    description: 'description',
    price: 2000
  }
];


beforeAll(async () => {
  await mongoose.connect(database);
  await mongoose.connection.db.collection('users').remove({});
  await mongoose.connection.db.collection('orders').remove({});
  await mongoose.connection.db.collection('products').remove({});
  ({data: {token: sellerToken}} = await axios.post(`${app}/auth/register`, orderSeller));
  ({data: {token: buyerToken}} = await axios.post(`${app}/auth/register`, orderBuyer));
  const [{ data: data1 }, {data: data2}] = await Promise.all(
    soldProducts.map(product => 
      axios.post(`${app}/product`, product, {
        headers: {
          authorization: `Bearer ${sellerToken}`
        }
      })
    )
  );
  boughtProducts = [data1, data2].map((product) => {
    product.owner = product.owner._id;
    return product;
  });
});

afterAll(async (done) => {
  await mongoose.connection.db.collection('users').remove({});
  await mongoose.connection.db.collection('orders').remove({});
  await mongoose.connection.db.collection('products').remove({});
  await mongoose.disconnect(done);
});

describe('ORDER', () => {
  it('should create order of all products', () => {
    const orderDTO = {
      products: boughtProducts.map(product => ({
        product: product._id,
        price: product.price,
        quantity: 1
      }))
    };

    return request(app)
      .post('/order')
      .set('Authorization', `Bearer ${buyerToken}`)
      .set('Accept', 'application/json')
      .send(orderDTO)
      .expect(({body}) => {
        orderId = body._id;
        expect(body.owner.username).toEqual(orderBuyer.username);
        expect(body.products).toHaveLength(boughtProducts.length);
        expect(body.products.map((productItem) => productItem.product)).toMatchObject(boughtProducts)
        expect(body.totalPrice).toEqual(boughtProducts.reduce((acc, product) => acc + product.price, 0))
      })
      .expect(HttpStatus.CREATED)
  });

  it('should list all orders of buyer', () => {
    return request(app)
      .get('/order')
      .set('Authorization', `Bearer ${buyerToken}`)
      .set('Accept', 'application/json')
      .expect(({body}) => {
        expect(body).toHaveLength(1);
        const order = body.find((order) => order._id === orderId)
        expect(order).toBeDefined();
        expect(order.products).toHaveLength(boughtProducts.length);
        expect(order.products.map((productItem) => productItem.product)).toMatchObject(boughtProducts)
      })
      .expect(HttpStatus.OK)
  })

});